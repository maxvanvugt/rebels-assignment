import React, { Component } from "react";
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";

import {connect} from "react-redux";
import Info from "../pages/show/info";
import Episode from "../pages/show/episode"

class ShowContainer extends Component {

  render() {
    return (
      <HashRouter>
        {/* <ul className="header">
          <li><NavLink to="/">Info</NavLink></li>
          <li><NavLink to="/episodes">Episodes</NavLink></li>
        </ul> */}
        <div className="show-container">
          <div className="show-container-content">
            <Route exact path="/" component={Info}/>
            <Route path="/episode/:id" component={Episode}/>
          </div>
        </div>
      </HashRouter>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      show: state.show
  };
};

export default  connect(mapStateToProps)(ShowContainer);
import React, { Component } from "react";
import parse from 'html-react-parser';

import {connect} from "react-redux";
import { fetchShowEpisode } from "../../../store/actions/showActions";

class Episode extends Component {
  async componentDidMount() {
    await this.props.fetchShowEpisode(this.props.match.params.id)
  }
  render() {
    return (
      <div>
        <div className="show-episode-container">
          <div className="show-episode-img-container">
            <button className="route-back-btn" onClick={() => this.props.history.goBack()}>
            <span class="material-icons">
              chevron_left
            </span>
            </button>
            <img src={this.props.show.episodeInfo.image.original} />
          </div>
          <div className="content-container">
          <h2>{ this.props.show.episodeInfo.name}</h2>
          { parse(this.props.show.episodeInfo.summary || '')}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      show: state.show
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchShowEpisode: (episodeId) => {
            dispatch(fetchShowEpisode(episodeId));
        }
    };
};
 
export default connect(mapStateToProps, mapDispatchToProps)(Episode);
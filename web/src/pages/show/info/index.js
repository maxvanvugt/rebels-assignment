import React, { Component } from "react";
import parse from 'html-react-parser';
import {
  NavLink
} from "react-router-dom"

import {connect} from "react-redux";
import { fetchShow } from "../../../store/actions/showActions";

class Info extends Component {
  componentDidMount() {
    this.props.fetchShow('6771')
  }
  renderEpisodeList = () => {
    return (
      <ul className="show-info-episodes-list">
        {this.props.show.info._embedded.episodes.map((episode) =>  <li key={episode.id}>
          <NavLink to={`/episode/${episode.id}`}>{episode.name}</NavLink>
         </li>)}
      </ul>
    )
  }
  
  render() {
    return (
      <div>
        <div className="show-info-container">
          <div className="show-info-img-container">
            <img src={this.props.show.info.image.original} />
          </div>
          <div className="content-container">            
            <h2>{this.props.show.info.name}</h2>
            {parse(this.props.show.info.summary || '')}
          </div>
        </div>
        <div className="content-container">
          <h2>Episodes</h2>
          {this.renderEpisodeList()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      show: state.show
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchShow: (showId) => {
            dispatch(fetchShow(showId));
        },
    };
};
 
export default connect(mapStateToProps, mapDispatchToProps)(Info);
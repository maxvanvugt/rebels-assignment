import {createStore, combineReducers, applyMiddleware} from "redux";
import { logger } from "redux-logger";
import thunk from "redux-thunk";

import show from "./reducers/showReducer";

export default createStore(
    combineReducers({
        show
    }),
    {},
    applyMiddleware(logger, thunk )
);
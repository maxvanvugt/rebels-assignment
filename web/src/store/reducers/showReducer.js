const showReducer = (state = {
  info: {
    summary: '',
    image: {
      original: '',
      medium: ''
    },
    _embedded: {
      episodes: []
    }
  },
  episodeInfo: {
    image: {
      original: ''
    },
    summary: ''
  }
}, action) => {
  switch (action.type) {
      case "FETCH_SHOW":
          state = {
              ...state,
              info: action.payload
          };
          break;
      case "FETCH_SHOW_EPISODE":
        state = {
          ...state,
          episodeInfo: action.payload

        }
        break;
  }
  return state;
};

export default showReducer;
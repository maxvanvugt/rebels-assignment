export function fetchShow (showId) {
  return dispatch => {
    return fetch(`http://api.tvmaze.com/shows/${showId}?embed=episodes`)
    .then(resp => {
      return resp.json()
    }).then(data => {
      console.log('data', data)
      dispatch({
        type: "FETCH_SHOW",
        payload: data
      });
    }).catch(err => {
      console.log('err', err)
    })
  };
}

export function fetchShowEpisode (episodeId) {
  return dispatch => {
    return fetch(`http://api.tvmaze.com/episodes/${episodeId}`)
    .then(resp => {
      return resp.json()
    }).then(data => {
      console.log('data', data)
      dispatch({
        type: "FETCH_SHOW_EPISODE",
        payload: data
      });
    }).catch(err => {
      console.log('err', err)
    })
  };
}

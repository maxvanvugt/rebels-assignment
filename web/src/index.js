import React from "react";
import ReactDOM from "react-dom";
import ShowContainer from "./containers/showContainer";
import "./style.css";

import {Provider} from "react-redux";
import store from "./store";

ReactDOM.render(
  <Provider store={store}>
    <ShowContainer/>
  </Provider>,
  document.getElementById("root")
);